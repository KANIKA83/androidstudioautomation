package com.example.kanikaagarwal.tests;

import com.example.kanikaagarwal.pages.LoginPage;

import org.testng.annotations.Test;

public class LoginTests {

    @Test
    public void testLogin(){
        LoginPage loginPage = new LoginPage();
        if(loginPage.validateLoginPage() == true) {
            loginPage.testLoginWithoutCredentials();
            System.out.println("Pass");
        }
        else{
            System.out.println("Validation Failed");
        }

    }

}
