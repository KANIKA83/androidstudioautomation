package com.example.kanikaagarwal.core;


import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseSetup {

    private DesiredCapabilities cap = new DesiredCapabilities();
    private static AndroidDriver androidDriver = null;

    private String appiumPort;
    private String serverIp;

    @BeforeClass
    public void setup() {

    }

    public AndroidDriver getDriver() {
        return androidDriver;
    }

    public void initDriver() {
        System.out.println("Inside initDriver method");

        File f = new File("src");
        File fs = new File(f, "ApiDemos-debug.apk");

        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
        cap.setCapability("noReset", true);
        String serverUrl = "http://" + serverIp + ":" + appiumPort + "/wd/hub";

        try
        {
            System.out.println("Argument to driver object : " + serverUrl);
            androidDriver = new AndroidDriver(new URL(serverUrl), cap);

        }
        catch (NullPointerException | MalformedURLException ex) {
            throw new RuntimeException("appium driver could not be initialised for device ");
        }
        System.out.println("Driver in initdriver is : "+androidDriver);

    }

    @AfterClass

    public void tearDown(){
        androidDriver.quit();
    }

}



