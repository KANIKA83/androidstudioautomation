package com.example.kanikaagarwal.pages;

import com.example.kanikaagarwal.core.Driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StaticContents extends Driver {

    PageObjects sc;


    public StaticContents() {
        super();
        sc = new StaticContents.PageObjects();
        PageFactory.initElements(driver, sc);
    }


    public boolean TandCText() {
        boolean elements = false;
        if (sc.TC.isDisplayed()) {
            if (sc.TC.getText() == "xyz") {
                elements = true;
            }
        } else {
            elements = false;
        }

        return elements;
    }

    class PageObjects {
        @FindBy(id = "change/reset_password_content")
        public WebElement changeResetPwdLink;

        @FindBy(id = "logo")
        public WebElement Viclogo;

        @FindBy(id = "T&C")
        public WebElement TC;
    }
}
